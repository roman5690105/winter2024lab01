import java.util.Random;

public class Calculator{
    public static int sumOfTwo(int x, int y){
        return x + y;
    }

    public static double squareIt(int num){
        return Math.sqrt(num);
    }

    public static int randomNumYes(){
        Random rand = new Random();
        return rand.nextInt(100);
    }

    public static int dividerYes(int num1, int num2){
        if(num2 == 0){
            return 0;
        }
        return num1/num2;
    }



}